<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchLog extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'search_logs';
	
	public $timestamps = true;
	
	
	static function addLog($keyword){
		$keyword = rawurldecode($keyword);
		
		try{
			$log = self::where('keyword',$keyword)->firstOrFail();
		}catch (\Exception $e){
			$log = New self;
			$log->keyword = $keyword;
		}
		
		$log->search_times += 1;
		$log->save();
	}
	
	
}
