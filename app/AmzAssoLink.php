<?php namespace App;

use DB;
use Illuminate\Config\Repository as Config;
use Illuminate\Http\Request;

class AmzAssoLink {

	protected $accounts = [
			'us'=>'quote003-20',
			'uk'=>'rs060-21',
			'es'=>'rs069-21',
			'fr'=>'rs06b-21',
			'de'=>'rs06e-21',
			'it'=>'rs071-21',
			'ca'=>'rs031-20'
	];
	
	
	
	protected $_country_code  = null;
	
    public function __construct( )
    {
        
       
        
        $this->_country_code = session('country_code_3',null);
    }

    /**
     * @return string|null
     */
    public function getCountryCodeFromClientIp()
    {
    	
    	
	        
    	if(empty($this->_country_code)) {
    		$ip = $this->getClientIP();
    		
    		$this->_country_code = $this->getCountryCodeFromIp($ip);
    		//echo '<div style="display:none">'.$ip.$this->_country_code.'</div>';
    		session(['country_code_3'=>$this->_country_code]);
    	}
	        
	    
    	return $this->_country_code;
    }

    /**
     * @param $ip
     * @return string|null
     */
    public function getCountryCodeFromIp($ip=null)
    {
    	if($ip == null) {
    		return NULL;
    	}
    	
    	
    	
    	$location = geoip()->getLocation($ip);
    	
    	if(empty($location)) {
    		return 'n/a';
    	}
    	
    	return strtolower($location->iso_code);
        return 'n/a';
    }
    
    
    public function link($asin){
    	$country = $this->getCountryCodeFromClientIp();
    	
    	switch ($country) {
    		case 'uk':
    		case 'gb':
    			return "https://www.amazon.co.uk/dp/$asin?tag=".$this->accounts['uk'];
    		case 'fr':
    			return "https://www.amazon.fr/dp/$asin?tag=".$this->accounts['fr'];
    		case 'es':
    			return "https://www.amazon.es/dp/$asin?tag=".$this->accounts['es'];
    		case 'it':
    			return "https://www.amazon.it/dp/$asin?tag=".$this->accounts['it'];
    		case 'de':
    		case 'at':
    		case 'nl':
    		case 'tr':
    		case 'ch':
    		case 'pt':
    		case 'lu':
    		case 'lv':
    			return "https://www.amazon.de/dp/$asin?tag=".$this->accounts['de'];
    		case 'ca':
    			return "https://www.amazon.ca/dp/$asin?tag=".$this->accounts['ca'];
    		default:
    			return "https://www.amazon.com/dp/$asin?tag=".$this->accounts['us'];
    	}
    }
    
    public function reviewlink($asin){
    	$country = $this->getCountryCodeFromClientIp();
    	
    	switch ($country) {
    		case 'uk':
    		case 'gb':
    			return "https://www.amazon.co.uk/review/product/$asin?tag=".$this->accounts['uk'];
    		case 'fr':
    			return "https://www.amazon.fr/review/product/$asin?tag=".$this->accounts['fr'];
    		case 'es':
    			return "https://www.amazon.es/review/product/$asin?tag=".$this->accounts['es'];
    		case 'it':
    			return "https://www.amazon.it/review/product/$asin?tag=".$this->accounts['it'];
    		case 'de':
    		case 'at':
    		case 'nl':
    		case 'tr':
    		case 'ch':
    		case 'pt':
    		case 'lu':
    		case 'lv':
    			return "https://www.amazon.de/review/product/$asin?tag=".$this->accounts['de'];
    		case 'ca':
    			return "https://www.amazon.ca/review/product/$asin?tag=".$this->accounts['ca'];
    		default:
    			return "https://www.amazon.com/review/product/$asin?tag=".$this->accounts['us'];
    	}
    }
    /**
     * Get the client IP address.
     *
     * @return string
     */
    public function getClientIP()
    {
    	$remotes_keys = [
    			'HTTP_X_FORWARDED_FOR',
    			'HTTP_CLIENT_IP',
    			'HTTP_X_FORWARDED',
    			'HTTP_FORWARDED_FOR',
    			'HTTP_FORWARDED',
    			'REMOTE_ADDR',
    			'HTTP_X_CLUSTER_CLIENT_IP',
    	];
    	foreach ($remotes_keys as $key) {
    		if ($address = getenv($key)) {
    			foreach (explode(',', $address) as $ip) {
    				if ($this->isValid($ip)) {
    					return $ip;
    				}
    			}
    		}
    	}
    	return '127.0.0.0';
    }
    /**
     * Checks if the ip is valid.
     *
     * @param string $ip
     *
     * @return bool
     */
    private function isValid($ip)
    {
    	if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)
    			&& !filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 | FILTER_FLAG_NO_PRIV_RANGE)
    			) {
    				return false;
    			}
    			return true;
    }
    

}