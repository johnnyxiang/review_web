<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\SearchController;
use DB;


class ProductAttr extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'product_attrs';
	
	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'asin';
	protected $keyType = 'string';
	
	public $timestamps = false;
	
	
	/**
	 * Get the order feedback associated with the order.
	 */
	public function product()
	{   
		return $this->hasOne('App\Product','asin', 'asin');
	}
	
	
}
