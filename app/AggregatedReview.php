<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\SearchController;
use DB;


class AggregatedReview extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reviews_aggregated';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'asin';
    protected $keyType = 'string';

    public $timestamps = false;


    /**
     * Get the order feedback associated with the order.
     */
    public function features()
    {
        return $this->hasMany('App\ProductFeature', 'asin', 'asin');
    }

    /**
     * Get the order feedback associated with the order.
     */
    public function product()
    {
        return $this->hasOne('App\Product', 'asin', 'asin');
    }

    public function related()
    {
        return $this->product->relatedProducts();
    }

    static public function relatedForProducts($products, $limit = 5)
    {
        $related_asins = [];
        $exludes = [];
        foreach ($products as $p) {
            $related_asins = array_merge($related_asins, $p->product->related());
            $exludes[] = $p->asin;
        }

        if (!empty($related_asins)) {
            $query = self::whereIn('asin', $related_asins)->whereNotIn('asin', $exludes)->orderBy('total_score', 'desc')->limit($limit);
            $related = $query->get();
        } else {
            $related = [];
        }

        return $related;
    }

    public function categories()
    {
        return $this->hasMany('App\CategoryProduct', 'asin', 'asin');
    }

    public function categoryTree()
    {
        return $this->product->categoryTree();
    }


    public function aScore()
    {
        //return $this->ascore;
        if ($this->pos_features + $this->neg_features == 0) {
            return 0;
        }

        $score = $this->average * 0.4 + $this->score * 0.4 + +0.2 * 5 * $this->pos_features / ($this->pos_features + $this->neg_features);

        return min($score * 1.03, 5);
        return (0.1 * 5 * atan(max(0, $this->total - 50)) * 2 / pi() + $this->score * 0.7 + 0.2 * 5 * $this->pos_features / ($this->pos_features + $this->neg_features));

        return (atan(max(0, $this->total - 50)) * 2 / PI()) * (($this->score * 0.9 + 0.1 * 5 * $this->pos_features / ($this->pos_features + $this->neg_features)));
    }

    public function topFeatures($order = "DESC", $limit = 1)
    {
        $query = $this->features()
            ->whereRaw('pos+neg >= 10')
            ->limit($limit);

        if ($order == "DESC") {
            $query->orderByRaw('pos-neg ' . $order);
        } else {
            $query->orderByRaw('pos/(pos+neg) ' . $order);
        }
// 		print($query->toSql());

        return $query->get();
    }


    public static function search($keyword, $limit = 5, $type = "search")
    {
        $keyword = trim($keyword);
        $keyword = str_replace("-n-", " ", $keyword);

        $query = self::join('products', function ($join) use ($keyword, $type) {
            $join->on('products.asin', '=', 'reviews_aggregated.asin');
        })->with(['product', 'features'])
            ->select(['ascore', 'products.asin', 'reviews_aggregated.asin', 'most_positive_words', 'most_negative_words', 'most_used_words'])
            ->where("reviews_aggregated.status", "=", "1")
            ->orderBy("sorter", "desc")
            ->limit($limit);


        if ($type == "brand") {
            if ($keyword != urldecode($keyword)) {
                $query->whereIn("brand", [$keyword, urldecode($keyword)]);
            } else {
                $query->where("brand", $keyword);
            }
        } else {
            $keyword = str_replace("+", " ", $keyword);
            $keyword = "+" . implode(" +", explode(" ", $keyword));
            $query->whereRaw("MATCH (name, brand) AGAINST ('{$keyword}'  IN BOOLEAN MODE )");
        }

//  		print($query->toSql());die();

        SearchLog::addLog($keyword);

        return $query->get();
    }

    public function positive_summaries()
    {
        return json_decode($this->positive_summary);
    }

    public function negative_summaries()
    {
        return json_decode($this->negative_summary);
    }
}
