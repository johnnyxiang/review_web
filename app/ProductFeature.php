<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFeature extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'product_features';
	
	public $timestamps = false;
	
	
	public function pos_summary(){
		
		$s = json_decode(str_replace("']", '"]',str_replace('u"','"',str_replace("',",'",',str_replace("u'",'"', $this->pos_summary)))),true);
// 		var_dump($s,$this->pos_summary);
		return $s[0]?$s[0]:"N/A";
	}
	
	public function neg_summary(){
		$s = json_decode(str_replace("']", '"]',str_replace('u"','"',str_replace("',",'",',str_replace("u'",'"', $this->neg_summary)))),true);
		return $s[0]?$s[0]:"N/A";
	}
	
}
