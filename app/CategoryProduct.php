<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'category_products';
	
	public $timestamps = false;
	
	
	public function product(){
		return $this->belongsTo('App\Product','asin', 'asin');
	}
	
}
