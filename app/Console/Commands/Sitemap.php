<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Category;
use App\Product;
use App;


class Sitemap extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate {type=category}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate xml sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
       
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$type = $this->argument('type');
    	
    	$this->info('Starting generate '.$type.' xml sitemap...');
    	
    	if($type == 'category' || $type == 'c') {
    		$this->generateCategorySitemap();
    	} elseif($type == 'product' || $type == 'p') {
    		Product::generateSitemap($this);
    	}
    	elseif($type == 'brand' || $type == 'b') {
    		Product::generateBrandSitemap($this);
    	}elseif($type == 'fix') {
    		$this->info(Category::isBroken());
    		Category::fixTree();
    		$this->info(Category::isBroken());
    	}
    	
    	$this->info('Finished generate '.$type.' xml sitemap...');
    }
    
    public function generateCategorySitemap() {
    	// create new sitemap object
    	Category::generateSitemap($this);
    }
}
