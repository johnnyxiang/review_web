<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Console\Command;
use DB;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    public $timestamps = false;

    protected $all_info_array = [];

    protected $attrs = ["Brand", "Model", "Color", "Size", "Dimensions", "Weight"];

    /**
     * Get the order feedback associated with the order.
     */
    public function features()
    {
        return $this->hasMany('App\ProductFeature', 'asin', 'asin');
    }

    /**
     * Get the order feedback associated with the order.
     */
    public function reviews()
    {
        return $this->hasMany('App\Review', 'asin', 'asin');
    }

    /**
     * Get the order feedback associated with the order.
     */
    public function aggregated_review()
    {
        return $this->hasOne('App\AggregatedReview', 'asin', 'asin');
    }


    public function info()
    {
        return $this->hasOne('App\ProductAttr', 'asin', 'asin');
    }


    public function pcategories()
    {
        return $this->hasMany('App\CategoryProduct', 'asin', 'asin');
    }

    private $_category_tree = NUll;

    public function categoryTree()
    {
        if (empty($this->_category_tree)) {

            $ids = [];
            foreach ($this->pcategories as $c) {
                $ids[] = $c->category_id;
            }
            $this->_category_tree = Category::whereIn('id', $ids)->get()->toTree();
        }
        return $this->_category_tree;
    }

    public function related()
    {
        if (empty($this->info->related) || $this->info->related == "N/A") {
            return [];
        }

        $asins = explode(", ", $this->info->related);

        return $asins;
    }

    function lastCategory($tree)
    {
        foreach ($tree as $cat) {
            $current_cat = $cat->toArray();

            while (true) {

                if (!empty($current_cat["children"])) {
                    foreach ($current_cat["children"] as $child) {
                        if (!empty($child["children"])) {
                            $current_cat = $child;
                            continue;
                        }
                    }

                    $current_cat = $current_cat["children"][0];

                } else {
                    return (object)$current_cat;
                }

            }
        }
    }

    public function alsoLikes()
    {
        $categories = $this->categoryTree()->toFlatTree();
        $category = $this->lastCategory($categories);

        $query = self::select(['asin', 'name', 'slug', 'image']);

        if (!empty($category)) {
            $query->whereHas('pcategories', function ($q) use ($category) {
                $q->where('category_id', $category->id);
            });
        } else {
            $query->where('brand', $this->brand);
        }

        $query->limit(18);

        return $query->get();
    }

    public function relatedProducts()
    {
        return [];
    }

    public function image()
    {

        if (!empty($this->image)) {
            return $this->image;
        }

        $images = $this->images();
        #var_dump($images);die();
        if (!empty($images)) {
            return $images[0];
        }

        return '';
    }


    public function getFeature()
    {
        return $this->info->features;
    }

    public function images()
    {
        return explode(",", $this->info->images);
    }

    public function brand_url()
    {
        return rawurlencode(strtolower(str_replace(" ", "-n-", $this->brand)));
    }

    public function attribute($attribute, $params = [])
    {
        if (method_exists($this, "get" . $attribute)) {
            return call_user_func_array(array($this, "get" . $attribute), array($params));
        }

        return false;
    }

    function getUPC()
    {
        return $this->info->upc;
    }

    function getBrand()
    {
        return $this->brand;
    }

    function getSize()
    {
        return $this->info->size;
    }

    function getASIN()
    {
        return $this->asin;
    }

    function getMPN()
    {
        return $this->info->mpn;
    }

    function getModel()
    {
        return $this->info->model;
    }

    function getColor()
    {
        return $this->info->color;
    }


    function getDimensions($params = [])
    {
        if (isset($params["unit"])) {
            $unit = $params["unit"];
        } else {
            $unit = "inches";
        }
        if (!empty($this->dimension)) {

            if ($unit == "cms") {
                $multi = 2.54;
            } else {
                $multi = 1;
            }

            $dd = [];
            foreach (["depth", "width", "height"] as $k) {
                if (!empty($this->info->{$k})) {
                    $dd[] = round($this->{$k} * $multi, 1);
                }
            }

            if (count($dd) == 0) {
                return false;
            }

            $dimension = implode(" x ", $dd) . " " . $unit;

            return $dimension;
        }

        return false;
    }

    function getWeight($params = [])
    {
        if (isset($params["unit"])) {
            $unit = $params["unit"];
        } else {
            $unit = "pounds";
        }

        if (!empty($this->info->weight)) {

            if ($unit == "kg") {
                $multi = 0.453592;
            } else {
                $multi = 1;
            }

            $weight = round($this->info->weight * $multi, 1) . " $unit";

            return $weight;
        }

        return false;
    }

    public function attrs($attrs = [])
    {
        $info = [];

        if (count($attrs) == 0) {
            $attrs = $this->attrs;
        }

        foreach ($attrs as $label => $attribute) {
            if ($v = $this->attribute($attribute)) {

                $info[is_int($label) ? $attribute : $label] = $v;
            }
        }
        return $info;
    }

    public function attrs_text($attrs = [], $delimiter = "<br/>", $format = "<strong>%s: </strong> %s")
    {
        $attrs = $this->attrs($attrs);

        $a = [];
        foreach ($attrs as $k => $v) {
            $a[] = sprintf($format, $k, $v);
        }

        return implode($delimiter, $a);
    }


    public function description()
    {
        return $this->info->description;
    }

    static public function generateBrandSitemap(Command $command = null)
    {
        $sitemap = App::make("sitemap");

        $limit = 20000;


        $total = self::where('status', '>=', 1)->where('slug', '<>', NULL)->count(DB::raw('distinct brand'));
        if ($command) {
            $command->info("Total $total urls to be added.");
        }

        $sitemapCounter = 1;

        for ($counter = 1; $counter <= ceil($total / $limit); $counter++) {

            if ($command) {
                $command->info('generating from ' . ($counter - 1) * $limit . ' to ' . ($counter * $limit));
            }

            $brands = \DB::table('products')
                ->select(DB::raw('distinct brand'))
                ->where('products.status', '=', 1)
                ->where('slug', '<>', NULL)
                ->orderBy("brand", "asc")
                ->limit($limit)->offset(($counter - 1) * $limit)->get();


            if (count($brands) == 0) {
                break;
            }

            foreach ($brands as $p) {
                // add product to items array
                $sitemap->add(route('brand.view', [rawurlencode($p->brand)]), date("c"), 0.9, 'weekly');
            }

            // generate new sitemap file
            $sitemap->store('xml', 'sitemap-brand-' . $sitemapCounter);
            // add the file to the sitemaps array
            $sitemap->addSitemap(secure_url('sitemap-brand-' . $sitemapCounter . '.xml'));
            // reset items array (clear memory)
            $sitemap->model->resetItems();

            if ($command) {
                $command->info('sitemap-brand-' . $sitemapCounter . '.xml generated');
            }

            $sitemapCounter++;
        }

        // you need to check for unused items
        if (!empty($sitemap->model->getItems())) {
            // generate sitemap with last items
            $sitemap->store('xml', 'sitemap-brand-' . $sitemapCounter);
            // add sitemap to sitemaps array
            $sitemap->addSitemap(secure_url('sitemap-brand-' . $sitemapCounter . '.xml'));
            // reset items array
            $sitemap->model->resetItems();
        }

        // generate new sitemapindex that will contain all generated sitemaps above
        $sitemap->store('sitemapindex', 'sitemap-brand');


    }

    static public function generateSitemap(Command $command = null)
    {
        // create new sitemap object
        $sitemap = App::make("sitemap");

        $limit = 5000;


        $total = self::where('status', '>=', 1)->where('slug', '<>', NULL)->count();
        if ($command) {
            $command->info("Total $total urls to be added.");
        }

        $sitemapCounter = 1;

        for ($counter = 1; $counter <= ceil($total / $limit); $counter++) {

            if ($command) {
                $command->info('generating from ' . ($counter - 1) * $limit . ' to ' . ($counter * $limit));
            }

            $products = self::where('status', '>=', 1)->where('slug', '<>', NULL)
                ->select(['slug', 'com_price_last_checked', 'review_last_checked'])
                ->limit($limit)->offset(($counter - 1) * $limit)->get();

            if (count($products) == 0) {
                break;
            }

            foreach ($products as $p) {
                // add product to items array
                $sitemap->add(route('product.view', [$p->slug]), date("c", strtotime($p->review_last_checked ? $p->review_last_checked : $p->com_price_last_checked)), 0.8);
            }


            // generate new sitemap file
            $sitemap->store('xml', 'sitemap-product-' . $sitemapCounter);
            // add the file to the sitemaps array
            $sitemap->addSitemap(secure_url('sitemap-product-' . $sitemapCounter . '.xml'));
            // reset items array (clear memory)
            $sitemap->model->resetItems();

            if ($command) {
                $command->info('sitemap-product-' . $sitemapCounter . '.xml generated');
            }

            $sitemapCounter++;
        }


        // you need to check for unused items
        if (!empty($sitemap->model->getItems())) {
            // generate sitemap with last items
            $sitemap->store('xml', 'sitemap-product-' . $sitemapCounter);
            // add sitemap to sitemaps array
            $sitemap->addSitemap(secure_url('sitemap-product-' . $sitemapCounter . '.xml'));
            // reset items array
            $sitemap->model->resetItems();
        }

        // generate new sitemapindex that will contain all generated sitemaps above
        $sitemap->store('sitemapindex', 'sitemap-product');
    }
}
