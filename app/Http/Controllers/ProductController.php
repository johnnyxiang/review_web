<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\AggregatedReview;
use App\Product;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    function viewAction($slug)
    {
        $product = Product::with(['aggregated_review', 'info'])->where('slug', '=', $slug)
            ->firstOrFail();
        return view('product.view', ["review" => $product->aggregated_review, "product" => $product]);
    }

    function sitemapAction()
    {
        $products = AggregatedReview::simplePaginate(5000);
        $content = View::make('product.sitemap')->with('products', $products);
        return Response::make($content, '200')->header('Content-Type', 'text/xml');
    }


    function removeAction($asin)
    {
        $this->middleware('auth');
        if (Auth::user()->role != 'admin') {
            abort(404, 'The page you are looking for could not be found');
        }

        Product::where('asin', $asin)->update(['status' => 0]);
        AggregatedReview::where('asin', $asin)->update(['status' => 0]);

        return redirect()->back();
    }

    function relatedAction(Request $request, $asin)
    {
        $product = Product::where('asin', '=', $asin)->firstOrFail();
        return view('product.parts.related-grid', ['items' => $product->alsoLikes(), "product" => $product]);
    }

}