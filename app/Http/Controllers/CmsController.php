<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Page;

class CmsController extends Controller
{
	function pageAction(Request $request,$slug){
		$page = Page::where('url_key','=',$slug)->firstOrFail();
		return view('cms.page',['page'=>$page]);
	}
}