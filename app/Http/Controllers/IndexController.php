<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;
use View;
use Response;

class IndexController extends Controller
{
	function indexAction( Request $request){
		$posts = Post::where("post_status",'=','publish')
		->orderBy("created_at","desc")
		->simplePaginate(20);

		return view('index.index',['posts'=>$posts,'ajax'=>$request->ajax()]);
	}
	
	
	public function logout(){
		Auth::logout();
		return redirect('home');
	}
	
	function sitemapAction(){
		$content = View::make('index.sitemap');
		return Response::make($content, '200')->header('Content-Type', 'text/xml');
	}
}