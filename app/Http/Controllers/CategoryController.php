<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\AggregatedReview;
use Kalnoy\Nestedset\Node;
use DB;
use View;
use Response;
use App\CategoryProduct;
use Illuminate\Support\Facades\Auth;
use App\Post;

class CategoryController extends Controller
{
    function viewAction($slug,$limit = 7)
    {
        $category = Category::withDepth()->where("slug", "=", $slug)->firstOrFail();

        $ancestors = [];

        $reviews = $category->topProducts($limit);

        if ($limit > count($reviews))
            $limit = count($reviews);

        return view('category.view', [
            "category" => $category,
            "reviews" => $reviews,
            "limit" => $limit,
            'related' => [],
            'ancestors' => $ancestors
        ]);
    }

    function catetoryTree($slug)
    {
        $category = Category::withDepth()->where("slug", "=", $slug)->firstOrFail();
        $printed = [];
        if (!isset ($category)) {
            $category = null;
        }
        $traverse = function ($categories, $prefix = '-') use (&$traverse, &$printed, $category) {
            if (count($categories) > 0) {
                echo "<ul>";
            } else {
                return;
            }
            foreach ($categories as $tcategory) {

                if (in_array($tcategory->id, $printed)) {
                    continue;
                }

                echo '<li><a title="' . $tcategory->name() . '" href="' . route('category.view', [
                        'slug' => $tcategory->slug
                    ]) . '">' . $tcategory->name() . '</a>';

                $printed [] = $tcategory->id;

                if (empty ($category) || ($category->id == $tcategory->id or $category->depth > $tcategory->depth)) {
                    $traverse ($tcategory->children, $prefix . '-');
                }
                echo '</li>';
            }

            echo "</ul>";
        };

        $categories = $category->categoryTree();
        if (count($categories) > 0) {
            echo '<section class="block clearfix">
				<h2 class="block-title">Related Categories</h2>
				<div class="block-content">';
            $traverse ($categories);
            echo ' </div></section>';
        }
    }

    function removeProductAction(Request $request, $cat_id, $asin)
    {
        $this->middleware('auth');
        if (Auth::user()->role != 'admin') {
            abort(404, 'The page you are looking for could not be found');
        }

        CategoryProduct::where('category_id', $cat_id)->where('asin', $asin)->update([
            'c_status' => 0
        ]);

        return redirect()->back();
    }

    function featureOnHomeAction(Request $request, $cat_id)
    {
        $this->middleware('auth');
        if (Auth::user()->role != 'admin') {
            abort(404, 'The page you are looking for could not be found');
        }

        $category = Category::where("id", "=", $cat_id)->firstOrFail();

        try {
            $post = Post::where("category_id", "=", $cat_id)->firstOrFail();
        } catch (\Exception $e) {
            $post = new Post ();
            $post->category_id = $cat_id;
            $post->post_title = "Top 5 Best " . $category->name();
            $p = $category->topProducts(1);
            if (count($p) > 0) {
                $post->image = $p [0]->product->image;
            }

            $post->save();

            session()->flash('msg', "Featured on homepage!");
        }

        return redirect()->back();
    }

    function removeFromHomeAction(Request $request, $post_id)
    {
        $this->middleware('auth');
        if (Auth::user()->role != 'admin') {
            abort(404, 'The page you are looking for could not be found');
        }

        Post::where('id', $post_id)->delete();

        return redirect()->back();
    }

    function sitemapAction()
    {
        $categories = Category::withDepth()->having('depth', '>', 1)->simplePaginate(500);

        $content = View::make('category.sitemap')->with('categories', $categories);
        return Response::make($content, '200')->header('Content-Type', 'text/xml');
    }
}