<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\AggregatedReview;
use Kalnoy\Nestedset\Node;
use DB;
use App\Product;
use Illuminate\Support\Facades\Auth;
use App\Post;

class SearchController extends Controller
{
    function resultAction(Request $request, $keyword = "")
    {

        if (empty($keyword)) {
            $keyword = $request->get("q");
        }

        if (empty($keyword)) {
            abort(404, 'The page you are looking for could not be found');
        }

        $limit = 7;

        $keyword = rawurldecode($keyword);

        try {
            $p = Product::where("asin", trim($keyword))->firstOrFail();
            return redirect()->route("product.view", ['slug' => $p->slug ? $p->slug : $p->asin]);
        } catch (\Exception $e) {

        }
        try {
            $categories = Category::search($keyword, $limit);
            if (count($categories) > 0) {
                if (count($categories) == 1) {
                    return redirect()->to($categories[0]->link());
                } else {
                    return view('search.category', ['keyword' => urldecode($keyword), 'categories' => $categories, "limit" => $limit]);
                }
            } else {
                throw new \Exception("No category found.");
            }
        } catch (\Exception $e) {
            $products = AggregatedReview::search($keyword, $limit);
            $related = AggregatedReview::relatedForProducts($products);
            return view('search.result', ['keyword' => urldecode($keyword), 'reviews' => $products, "limit" => $limit, 'related' => $related]);
        }
    }


    function brandAction($brand)
    {
        if (empty($brand)) {
            abort(404, 'The page you are looking for could not be found');
        }

        $brand = rawurldecode($brand);
        $brand = str_replace("-n-", " ", $brand);

        $limit = 7;
        $products = AggregatedReview::search($brand, $limit, 'brand');
        $related = AggregatedReview::relatedForProducts($products);

        return view('search.result', ['keyword' => ucwords($brand), 'reviews' => $products, "limit" => $limit, "type" => "brand", 'related' => $related]);
    }


    function featureOnHomeAction($keyword, $asin)
    {

        $this->middleware('auth');
        if (Auth::user()->role != 'admin') {
            abort(404, 'The page you are looking for could not be found');
        }

        $keyword = rawurldecode($keyword);

        $url = route("search.result", [rawurlencode($keyword)]);
        $url = str_replace(url("/"), "", $url);

        try {
            Post::where("page_url", "=", $url)->firstOrFail();
        } catch (\Exception $e) {
            $post = New Post();
            $post->page_url = $url;
            $post->post_title = "Top 5 Best Products for " . $keyword;
            $p = Product::where('asin', $asin)->firstOrFail();
            if (count($p) > 0) {
                $post->image = $p->image;
            }
            $post->save();
            session()->flash('msg', "Featured on homepage!");
        }

        return redirect()->back();
    }

}