<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_posts';
	
	public $timestamps = true;
	
	public function category(){
		return $this->belongsTo('App\Category','category_id', 'id');
	}
	
	public function title() {
		if(!empty( $this->post_title)) {
			return $this->post_title;
		}
		
		if(!empty($this->category)) {
			return "Top 5 Best ".$this->category->name();
		}
		
	}
	
	public  function image() {
		if(!empty($this->image)) {
			return $this->image;
		}
		
		if(!empty($this->category)) {
			$p = $this->category->topProducts(1);
			if(count($p) > 0) {
				return $p[0]->product->image;
			}
		}
		
		
		
	}
	
	public function link() {
		
		if($this->page_url) {
			if(strpos($this->page_url, "//") === false) {
				return url("/").$this->page_url;
			}
			
			return $this->page_url;
		}
		
		if(!empty($this->category)) {
			return route('category.view',['slug'=>$this->category->slug]);
		}
		
		return route('blog.view',['slug'=>$this->slug]);
	}
}
