<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\SearchController;
use DB;


class ProductInfo extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'more_info';
	
	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'asin';
	protected $keyType = 'string';
	
	public $timestamps = false;
	
	
	/**
	 * Get the order feedback associated with the order.
	 */
	public function product()
	{   
		return $this->hasOne('App\Product','asin', 'asin');
	}
	
	
}
