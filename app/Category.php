<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Kalnoy\Nestedset\NodeTrait;
use DB;
use Illuminate\Console\Command;

class Category extends Model
{
    use NodeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $fillable = ['name', 'slug'];

    protected $_image;


    /**
     * Get the order feedback associated with the order.
     */
    public function aggregated_reviews()
    {
        return $this->belongsToMany('App\AggregatedReview', 'category_products', 'category_id', 'asin');
    }

    public function categoryTree()
    {
        $result = Category::withDepth()->find($this->id);

        if ($result->depth >= 2 && $p = $this->parent) {
            $tree = self::withDepth()->limit(100)->descendantsAndSelf($p)->toTree();
        } else {
            $tree = self::withDepth()->limit(100)->descendantsAndSelf($this)->toTree();
        }

        return $tree;
    }


    public function topProducts($limit = 5)
    {
        $query = AggregatedReview::with(['product'])
            ->whereHas('categories', function ($q) {
                $q->where('category_id', $this->id)->where("c_status", "=", 1);
            })
            //->select(['ascore', 'reviews_aggregated.asin', 'most_positive_words', 'most_negative_words', 'most_used_words'])
            ->orderBy("sorter", "desc")
            ->limit($limit);
        return $query->get();
    }

    public function name()
    {
        if (!empty($this->alias)) {
            return $this->alias;
        }

        return $this->name;
    }

    function desc()
    {
        if (empty($this->desc)) {
            return "";
        }

        if (strpos($this->desc, "</p>") !== false || strpos($this->desc, "<br>") !== false || strpos($this->desc, "<br/>") !== false) {
            return $this->desc;
        }

        return nl2br($this->desc);
    }

    public function image()
    {
        if (empty($this->_image)) {
            $p = $this->topProducts(1);
            if (count($p) > 0) {
                $this->_image = $p[0]->product->image();
            }
        }

        return $this->_image;
    }

    public function link()
    {
        return route("category.view", ['slug' => $this->slug]);
    }

    public static function search($keyword, $limit = 5, $type = "search")
    {
        $keyword = trim($keyword);
        $keyword = str_replace("-n-", " ", $keyword);
        $keyword = str_replace("+", " ", $keyword);

        $keyword = "+" . implode("* +", explode(" ", $keyword)) . "*";

        $query = self::withDepth()
            ->selectRaw("MATCH (name, alias) AGAINST ('{$keyword}'  IN BOOLEAN MODE )  as relevance")
            ->having('depth', '>', 2)->whereRaw("MATCH (name, alias) AGAINST ('{$keyword}'  IN BOOLEAN MODE )")->orderBy("relevance", "DESC");
        return $query->get();

    }


    static public function generateSitemap(Command $command = null)
    {
        // create new sitemap object
        $sitemap = App::make("sitemap");
        $limit = 1000;

        $totalQ = self::withDepth();
        $total = $totalQ->count();

        if ($command) {
            $command->info("Total $total urls to be added.");
        }

        $sitemapCounter = 1;

        for ($i = 1; $i <= ceil($total / $limit); $i++) {

            if ($command) {
                $command->info('generating from ' . ($i - 1) * $limit . ' to ' . ($i * $limit));
            }

            $categories = self::withDepth()->having('depth', '>', 1)->limit($limit)->offset(($i - 1) * $limit)->get();

            if (count($categories) == 0) {
                break;
            }

            foreach ($categories as $cat) {
                // add product to items array
                $sitemap->add(route('category.view', [$cat->slug]), date("c"), 0.9, 'weekly');
            }


            $sitemap->store('xml', 'sitemap-category-' . $sitemapCounter);
            $sitemap->addSitemap(secure_url('sitemap-category-' . $sitemapCounter . '.xml'));
            $sitemap->model->resetItems();

            if ($command) {
                $command->info('sitemap-category-' . $sitemapCounter . '.xml generated');
            }
            $sitemapCounter++;
        }


        // you need to check for unused items
        if (!empty($sitemap->model->getItems())) {
            $sitemap->store('xml', 'sitemap-category-' . $sitemapCounter);
            $sitemap->addSitemap(secure_url('sitemap-category-' . $sitemapCounter . '.xml'));
            $sitemap->model->resetItems();
        }

        $sitemap->store('sitemapindex', 'sitemap-category');
    }
}
