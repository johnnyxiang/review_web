<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'IndexController@indexAction']);
Route::get('p/{slug}', ['as' => 'product.view', 'uses' => 'ProductController@viewAction']);
Route::get('p/remove/{asin}', ['as' => 'product.remove', 'uses' => 'ProductController@removeAction']);
Route::get('p/related/{asin}', ['as' => 'product.related', 'uses' => 'ProductController@relatedAction']);

Route::get('c/{slug}', ['as' => 'category.view', 'uses' => 'CategoryController@viewAction']);
Route::get('c/tree/{slug}', ['as' => 'category.treeview', 'uses' => 'CategoryController@catetoryTree']);
Route::get('cr/{cat}/{asin}', ['as' => 'category.remove_asin', 'uses' => 'CategoryController@removeProductAction']);
Route::get('c/feature/{cat}', ['as' => 'category.feature', 'uses' => 'CategoryController@featureOnHomeAction']);
Route::get('c/remove_feature/{post_id}', ['as' => 'category.remove_feature', 'uses' => 'CategoryController@removeFromHomeAction']);


Route::get('search/{keyword?}', ['as' => 'search.result', 'uses' => 'SearchController@resultAction']);
Route::get('search/feature/{keyword}/{asin}', ['as' => 'search.feature', 'uses' => 'SearchController@featureOnHomeAction']);
Route::get('brand/{brand}', ['as' => 'brand.view', 'uses' => 'SearchController@brandAction']);


Route::get('blog/{slug}', ['as' => 'blog.view', 'uses' => 'BlogController@viewAction']);
Route::get('page/{slug}', ['as' => 'cms.page', 'uses' => 'CmsController@pageAction']);

// Route::get('/sitemap/category.xml', ['as' => 'sitemap.category', 'uses' => 'CategoryController@sitemapAction']);
Route::get('/sitemap.xml', ['as' => 'sitemap', 'uses' => 'IndexController@sitemapAction']);
Route::auth();

Route::get('/home', 'IndexController@indexAction');
// Route::get('/logout',  [ "as" => "logout",'middleware' => 'auth', 'uses' => 'IndexController@logout']);
Route::get('/logout', 'Auth\LoginController@logout');
