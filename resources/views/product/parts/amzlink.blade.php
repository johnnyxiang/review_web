<?php 
$accounts = [
		'com'=>'quote003-20',
		'uk'=>'rs060-21',
		'es'=>'rs069-21',
		'fr'=>'rs06b-21',
		'de'=>'rs06e-21',
		'it'=>'rs071-21',
		'ca'=>'rs031-20'
];

if(!isset($style)) $style = "btn btn-warning btn-lg";
if(!isset($text)) $text = "Check Price on Amazon";
$link = AmzAssoLink::link($asin);
?>
<a class="{{$style}}" onclick="trackOutboundLink('{{$link}}')" target="_blank" ref="nofollow" href="{{$link}}" title="{{isset($title)?$title:$text}}">{!!$text!!}</a>