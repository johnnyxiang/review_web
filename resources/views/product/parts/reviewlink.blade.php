<?php 
if(!isset($style)) $style = "";
if(!isset($text)) $text = "Read all customer reviews on Amazon";
$link = AmzAssoLink::reviewlink($asin);
?>
<a class="{{$style}}" onclick="trackOutboundLink('{{$link}}')" target="_blank" ref="nofollow" href="{{$link}}" title="{{isset($title)?$title:$text}}">{!!$text!!}</a>