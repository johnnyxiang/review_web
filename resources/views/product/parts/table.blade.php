 @if(count($reviews) > 3)
 	<div style="overflow-x: auto">
      <div class="hidden-sm hidden-xs"  style="width: {{100*count($reviews)/7}}%">
       <table class="table table-bordered table-striped text-center">
       		<tr>
       			@foreach($reviews as $i=>$review)
       			<td><h5><a title="{{$review->product->name}}" href="{{route('product.view',[$review->product->slug])}}">#{{$i+1}} {{ str_limit($review->product->name, $limit = 100, $end = '...') }}</a></h5></td>
       			@endforeach
       		</tr>
       		<tr>
       			@foreach($reviews as $i=>$review)
       			<td>
					@include('product.parts.amzlink',['asin'=>$review->asin,'title'=>$review->product->name,'text'=>'<img  class="img-responsive table-img" alt="'.$review->product->name.'" src="'.$review->product->image().'">','style'=>''])
				</td>
       			@endforeach
       		</tr>
       		
       		
       		
       		<tr>
       			@foreach($reviews as $i=>$review)
       			<td>
       			Editor Rating<br/>
       			@include("product.parts.rating",['rate'=>$review->aScore() * 20])<br/>
       			{{round($review->aScore(),2)}} out of 5</td>
       			@endforeach
       		</tr>
       		
<!--       		<tr>-->
<!--       			@foreach($reviews as $i=>$review)-->
<!--       			<td>{!! $review->product->attrs_text(['Dimensions'],'<br/> ','%s <br/> %s') !!}</td>-->
<!--       			@endforeach-->
<!--       		</tr>-->
<!--       		<tr>-->
<!--       			@foreach($reviews as $i=>$review)-->
<!--       			<td>{!! $review->product->attrs_text(['Weight'],'<br/> ','%s <br/> %s') !!}</td>-->
<!--       			@endforeach-->
<!--       		</tr>-->
       		
       		<tr>
       			@foreach($reviews as $i=>$review)
       			<td>
       			@include('product.parts.amzlink',['asin'=>$review->asin,'text'=>'Check Price on Amazon','style'=>'btn btn-primary btn-sm'])
       			</td>
       			@endforeach
       		</tr>
       		
       		<tr>
       			@foreach($reviews as $i=>$review)
       			<td><a title="Read Full Report" href="{{ route('product.view',['slug'=>$review->product->slug])}}">Read Full Report</a></td>
       			@endforeach
       		</tr>
       		
       		@if (Auth::check() && Auth::user()->role == 'admin')
       		<tr>
       			@foreach($reviews as $i=>$review)
       			<td>
       			@if(isset($category)) 
       			<a  onclick="return confirm('Please confirm!')" href="{{ route('category.remove_asin',['category'=>$category->id,'asin'=>$review->asin])}}">Remove From Category</a><br/>
       			@endif
       			
       			<a  onclick="return confirm('Please confirm!')" href="{{ route('product.remove',['asin'=>$review->asin])}}">Remove Product</a>
       			</td>
       			@endforeach
       		</tr>
       		@endif
       
       </table>
       </div>
 </div>
 @endif