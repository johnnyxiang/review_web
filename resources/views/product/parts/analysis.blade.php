@if(!empty($review))
			
					
			@if(!empty($review->most_used_words) && !empty(json_decode($review->most_used_words,true)))	
			<h4>Most Used Words</h4>
			<div id="most_words"></div>
			<br/>
			@endif
			
			
			@if ($words = json_decode($review->most_positive_words,true))
			<h4>The Good</h4>
			
			
				<div class="tagcloud05 clearfix">
				
				<ul>@foreach( $words as $i=> $v)
				<li><span>{{$v[0]}} <span>({{$v[1]}})</span></span></li>
				@endforeach
				</ul>
				</div>
			@endif
			

			@if ($ss = json_decode($review->positive_summary,true))
				<ul>
				@foreach($ss as $s)
				<li>{{$s}}</li>
				@endforeach
				</ul>


			@endif

			
			@if ($words = json_decode($review->most_negative_words,true))
			<h4>The Bad</h4>
			
			
				<div class="tagcloud05 clearfix">
				
				<ul>@foreach($words as $i=> $v)
				<li><span>{{$v[0]}} <span>({{$v[1]}})</span></span></li>
				@endforeach
				</ul>
				</div>
			
			@endif

			@if ($ss = json_decode($review->negative_summary,true))
				<ul>
				@foreach($ss as $s)
				<li>{{$s}}</li>
				@endforeach
				</ul>

			@endif

			
			
		@if(count($review->features) > 0 )
		
		<?php /*?>
		<h4>Analysis By Features</h4>
			
			@foreach($review->features as $i=>$feature)
			@if($i <5)
				<p style="margin-top:20px;margin-bottom:5px;font-size:120%"><strong>{{$feature->feature}}</strong> </p>
				<p class="">
				<strong> Editor Rating:</strong> {{ round(100 *$feature->pos/($feature->pos+$feature->neg),1)}}%<br/>
				<strong>Good:</strong>  {{$feature->pos_summary()}} <br/>
				<strong> Bad:</strong> {{$feature->neg_summary()}} <br/>
				</p>
			@endif
			@endforeach
			
			<p class="text-center">@include('product.parts.amzlink',['asin'=>$product->asin])</p>
		 <? */ ?>
		@endif
		
		
		
		
		@endif