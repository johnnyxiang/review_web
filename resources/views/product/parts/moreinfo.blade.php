	@if(!empty( $product->description()) &&  $product->description()!= "N/A")
		<h4>Product Description</h4>
				
			{!! $product->description() !!}
				
		@endif
		
		@include("product.parts.videos")
				
		<h4>More Information</h4>
		
		{!! $product->attrs_text(['Brand','Model','MPN','Color','Dimensions','Weight','Size','UPC'],'<br/>') !!}
		
		

		<p class="text-center "><br> @include('product.parts.amzlink',['asin'=>$product->asin])</p>

		
		<h4>Categories</h4>
		
		@include("product.parts.breadcrumbs")		

