<div id="related-videos">
<h4>Related Videos</h4>
<div class="row">

</div>
<div class="text-center  main-action-btn">@include('product.parts.amzlink',['asin'=>$product->asin,'text'=>"Buy Now <small>on Amazon</small>"])</div>

</div>
<script>
jQl.loadjQdep('{{url('//cdn.rawgit.com/noelboss/featherlight/1.7.6/release/featherlight.min.js')}}')
jQuery(document).ready(function(){
	@if(strlen($product->name) > 80)
		var category = $(".top .breadcrumbs .breadcrumb:visible .breadcrumb-item").last().find("a").html()
		
		$.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=8&q={{urlencode($product->brand)}} "+category+"&key=AIzaSyDc3cPElXg3RynTIfCJ7ukUjTV3YknyRps",function(response){
//	 		var data = jQuery.parseJSON(response)
			var data = response
			
				if(data["items"].length > 0) {

					$.each(data["items"],function(k,item){
						if(item["id"]["kind"]!= "youtube#video") {
							return
						}
						var div = $('<div>').addClass("col-sm-3 item")
						var a = $('<a>').addClass("fl").attr("href","#").attr("data-featherlight","//www.youtube.com/embed/"+item["id"]["videoId"]+"?enablejsapi=1")
						var img = $("<img>")
						//console.log(item["snippet"])
						img.addClass("img-responsive").attr("src",item["snippet"]["thumbnails"]["medium"]["url"])
						a.append(img)
						a.append("<span class='duration'>&nbsp;&nbsp;▶&nbsp;&nbsp;</span>")
						div.append(a)
						$("#related-videos .row").append(div)
						
						a.featherlight({type: 'iframe',iframeMaxWidth: '100%', iframeWidth: 640,iframeHeight: 360});
						
					})

				$("#related-videos").show()
			}
		})
	@elseif($product->brand)
		$.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=8&q={{urlencode($product->brand." ".$product->name)}}&key=AIzaSyDc3cPElXg3RynTIfCJ7ukUjTV3YknyRps",function(response){
//	 		var data = jQuery.parseJSON(response)
			var data = response
			
			if(data["items"].length > 0) {
				
				$.each(data["items"],function(k,item){
					if(item["id"]["kind"]!= "youtube#video") {
						return
					}
					
					var div = $('<div>').addClass("col-sm-3 item")
					var a = $('<a>').addClass("fl").attr("href","#").attr("data-featherlight","//www.youtube.com/embed/"+item["id"]["videoId"]+"?enablejsapi=1")
					var img = $("<img>")
					//console.log(item["snippet"])
					img.addClass("img-responsive").attr("src",item["snippet"]["thumbnails"]["medium"]["url"])
					a.append(img)
					a.append("<span class='duration'>&nbsp;&nbsp;▶&nbsp;&nbsp;</span>")
					div.append(a)
					$("#related-videos .row").append(div)
					
					a.featherlight({type: 'iframe',iframeMaxWidth: '100%', iframeWidth: 640,iframeHeight: 360});
					
				})

				$("#related-videos").show()
			} 
	})

	@endif
})

</script>