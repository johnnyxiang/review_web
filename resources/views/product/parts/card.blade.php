<style>
    .p .img-thumbnail img{
        width: auto;
        max-height: 300px;
    }
</style>
<h2><a title="{{$review->product->name}}" href="{{route('product.view',[$review->product->slug])}}">{{$i+1}}. {{$review->product->name}}</a></h2>

<div class="row p">
			<div class="col-xs-12  col-md-4 pull-right text-center">
				@include("product.parts.gallary",['product'=>$review->product,'amz_link'=>true])
			</div>

			<div class="col-xs-12 col-md-8">
				<p class="">
					<strong>Brand:</strong> <a title="{{$review->product->brand}}" href="{{route("brand.view",[$review->product->brand_url()])}}">{{$review->product->brand}}</a>, {!! $review->product->attrs_text(['Color','Dimensions','Weight'],', ') !!}
				</p>

				@if($features = $review->product->getFeature())

				<ul style="margin-top: 15px;">
					@if(is_array($features))
					@foreach ($features as $i=> $v)
					@if($i>3) @break @endif
					<li>{!! $v   !!}</li>
					@endforeach
					@else
					<li>{!! $features   !!}</li>
					@endif
				</ul>
				@endif

				@if($review->aScore()>0)
				<h4>Analysis Overview</h4>
				<ul>
				 	@if($review->aScore()>0)
				 	<li><strong>Editor Rating: </strong> @include("product.parts.rating",['rate'=>round($review->aScore() * 20,0)]) <small>({{number_format($review->aScore(),2)}} out of 5)</small> </li>
				 	@endif

				 @foreach($review->features as $i=>$feature)
					@if($i>3) @break @endif

						@if(isset($category) && !empty($category->features))
							@if(in_array($feature->feature,explode(', ',$category->features)))
							<li><strong>{{ucfirst($feature->feature)}}: </strong>

							@include("product.parts.rating",['rate'=>round(100 * $feature->pos/($feature->pos+$feature->neg),1)])

							<small>{{ round(100 * $feature->pos/($feature->pos+$feature->neg),1)}}% ({{$feature->pos}} out of {{ ($feature->pos + $feature->neg) }})</small></li>
							@endif


						@else
							<li><strong>{{ ucfirst($feature->feature)}}: </strong>
							@include("product.parts.rating",['rate'=>round(100 * $feature->pos/($feature->pos+$feature->neg),1)])
							<small>({{ round( 5*$feature->pos/($feature->pos+$feature->neg),2)}} out of 5)</small>
							</li>
						@endif




					@endforeach
					</ul>
				@endif
				</div>

			<div class="col-xs-12  hidden-xs">
			<p></p>
			<p class="text-center">@include('product.parts.amzlink',['asin'=>$review->product->asin])</p>
			</div>

			<div class="col-xs-12">


				@if ($words = json_decode($review->most_positive_words,true))
				<h4>Customers are happy about:</h4>

				<div class="tagcloud05 clearfix">

				<ul>@foreach( $words as $i=> $v)
				<li><span>{{$v[0]}} <span>({{$v[1]}})</span></span></li>
				@endforeach
				</ul>
				</div>

				@endif

				@if ($words = json_decode($review->most_negative_words,true))
				<h4>Customers are unhappy about:</h4>

				<div class="tagcloud05 clearfix">

				<ul>@foreach($words as $i=> $v)
				<li><span>{{$v[0]}} <span>({{$v[1]}})</span></span></li>
				@endforeach
				</ul>


				</div>

				@endif

				@if($review->aScore()>0 && json_decode($review->most_positive_words,true) )
				<p class="text-center hidden" style="margin-top: 20px">
					@include('product.parts.amzlink',['asin'=>$review->asin])
					</p>
			   @endif


			</div>
</div>
