@if(count($items) > 0 )
	<h4>You May Also Like</h4>
	<div class="row grid">
    @foreach($items as $p)
    
    <?php
    	if(!empty($p->product)) $p = $p->product;
    ?>
    <div class="col-xs-6 col-sm-3 col-md-2 ">
    <div class="product-grid-container">
    <div class="product-grid-inner">
    <a  href="{{route('product.view',[$p->slug])}}" title="{{$p->name}}">
    <img class="img-responsive lazy" alt="{{$p->name}}" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-original="{{$p->image()}}">
    </a>
    
    
    </div>
   	</div>
   	<h5 class="product-name"><a title="{{$p->name}}"  href="{{route('product.view',[$p->slug])}}">{{ str_limit($p->name, $limit = 70, $end = '...') }}</a></h5>
   	</div>
    @endforeach
     </div>
   
 @endif
 <script type="text/javascript">
jQuery(document).ready(function(){

	function initLazyload(){
		if($("img.lazy").lazyload){
		 	$("img.lazy").lazyload()
		} else {
			setTimeout( initLazyload, 20 );
		}
	}

	initLazyload()
})
</script>
