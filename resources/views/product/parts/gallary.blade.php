<div class="img-thumbnail">
					<div id="fotorama{{$product->id}}" data-auto="false" >
					@if(isset($amz_link) && $amz_link ==true)
						@include('product.parts.amzlink',['asin'=>$product->asin,'style'=>'','title'=>$product->name,'text'=>'<img class="img-responsive lazy"  alt="'.$product->name.'" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-original="'.$product->image().'">'])
					@else	
						@if(!isset($show_gallery) || $show_gallery==false)	
						<a title="{{$product->name}}" href="{{route('product.view',[$product->slug])}}">
						@endif
                            <img class="img-responsive lazy"  alt="{{$product->name}}" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-original="{{$product->image()}}">
						@if(!isset($show_gallery) || $show_gallery==false)
						</a>
						@endif
					@endif
						</div>
					
</div>		
<p></p>
<p class="text-center hidden-md hidden-lg hidden-sm">@include('product.parts.amzlink',['asin'=>$product->asin,'text'=>"Check on Amazon"])</p>
					
@if(isset($show_gallery))					
<script>
jQuery(document).ready(function(){
							var data = [{img: '{{$product->image()}}'}]
							
							@foreach($product->images() as $image)
							@if($image != $product->image) 
								data.push({img: '{{$image}}'})
							@endif
							@endforeach


							function initGallery{{$product->id}}(){
								if($('#fotorama{{$product->id}}').fotorama){
									
									var fotorama{{$product->id}}Div = $('#fotorama{{$product->id}}').fotorama({
										  maxheight: 400,
										  maxwidth: '100%',
										  data:data,
										  allowfullscreen: true,
										  navposition:'left',
										  nav: 'thumbs'
										});

									var fotorama{{$product->id}} = fotorama{{$product->id}}Div.data('fotorama')
									
									fotorama{{$product->id}}Div.on("click",".fotorama__img",function(){
										
										fotorama{{$product->id}}.requestFullScreen()
										
									})
									
								} else {
									setTimeout( initGallery{{$product->id}}, 20 );
								}
							}

							initGallery{{$product->id}}()
							
													
							
							
})
</script>	
@endif			
						