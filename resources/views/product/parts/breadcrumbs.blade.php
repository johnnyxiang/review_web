<?php
$printed = [];
if (!isset($category)) {
    $category = null;
}
$traverse = function ($categories, $prefix = '-') use (&$traverse, &$printed, $category) {
// 	var_dump($categories);
    foreach ($categories as $tcategory) {

        if (in_array($tcategory->id, $printed)) {
            continue;
        }

        echo '<li class="breadcrumb-item"><a title="' . $tcategory->name() . '" href="' . route('category.view', ['slug' => $tcategory->slug]) . '">' . $tcategory->name() . '</a>';

        $printed[] = $tcategory->id;

        if (empty($category) || ($category->id == $tcategory->id or $category->depth > $tcategory->depth)) {
            $traverse($tcategory->children, $prefix . '-');
        }
        echo '</li>';

    }

    //echo "</ul>";
};
?>
<div class="breadcrumbs">
    @foreach($product->categoryTree() as $cat)
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a title="{{$cat->name()}}" href="{{route('category.view',['slug'=>$cat->slug])}}">{{$cat->name()}}</a>
        </li>
        {{$traverse($cat->children)}}
    </ul>
    @endforeach
</div>