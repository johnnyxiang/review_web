<section class="block clearfix">
	<h2 class="block-title">Related Products</h2>
	<div class="block-content">
	
	@if(count($items) > 0 )
    <div class="row grid">
    @foreach($items as $p)
   
    <?php if(!empty($p->product)) $p = $p->product; ?>
    <div class="col-xs-12">
    <div class="product-grid-container">
    <div class="product-grid-inner">
    <a href="{{route('product.view',[$p->slug])}}" title="{{$p->name}}">
    	<img class="img-responsive lazy" alt="{{$p->name}}" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-original="{{$p->image()}}" />
    </a>
    </div>
   	</div>
    <h5><a title="{{$p->name}}" href="{{route('product.view',[$p->slug])}}">{{ str_limit($p->name, $limit = 100, $end = '...') }}</a></h5>
    </div>
    @endforeach
    </div>
    @else
    @include('product.parts.amzads')
    @endif
    </div>
</section>

