@if(isset($review) && $review->aScore()>0)
<h4>Analysis Overview</h4>
<ul>
    @if($review->aScore()>0)
    <li><strong>Overall Rating: </strong> @include("product.parts.rating",['rate'=>round($review->aScore() * 20,0)])
        <small>({{number_format($review->aScore(),2)}} out of 5)</small>
    </li>
    @endif


    @foreach($review->features as $i=>$feature)
    @if($i>5) @break @endif

    @if(isset($category) && !empty($category->features))
    @if(in_array($feature->feature,explode(', ',$category->features)))
    <li><strong>{{ucfirst($feature->feature)}}: </strong>

        @include("product.parts.rating",['rate'=>round(100 * $feature->pos/($feature->pos+$feature->neg),1)])

        <small>{{ round(100 * $feature->pos/($feature->pos+$feature->neg),1)}}% ({{$feature->pos}} out of {{ ($feature->pos + $feature->neg)
            }})
        </small>
    </li>
    @endif

    @else
    <li><strong>{{ ucfirst($feature->feature)}}: </strong>
        @include("product.parts.rating",['rate'=>round(100 * $feature->pos/($feature->pos+$feature->neg),1)])
        <small>({{ round( 5*$feature->pos/($feature->pos+$feature->neg),2)}} out of 5)</small>
    </li>
    @endif
    @endforeach
</ul>
@else
<h4>More Information</h4>
{!! $product->attrs_text(['Brand','Model','MPN','Color','Dimensions','Weight','Size','UPC'],'<br/>') !!}
@endif
<div class="text-center  main-action-btn">@include('product.parts.amzlink',['asin'=>$product->asin,'text'=>"Buy Now
    <small>on Amazon</small>
    "])
</div>
<div class="text-center  main-action-btn">@include('product.parts.reviewlink',['asin'=>$product->asin])</div>