@extends('layouts.full')
@section('title',$product->name )
@section('meta_description',"Looking for ".$product->name."? Check out our latest review analysis and buy with confidence.")
@section('content')


<div class="product-main">
		<div class="top" style="margin-bottom:20px">
		@include("product.parts.breadcrumbs")
		</div>
		<div class="row">

			<div class="col-md-4 col-sm-6 text-center">
                @include("product.parts.gallary",['product'=>$product,'show_gallery'=>true])
			</div>

			<div class="col-md-5 col-sm-6">
				<h1 class="title">
				@include('product.parts.amzlink',['asin'=>$product->asin,'text'=>$product->name,'style'=>''])
				</h1>

                <div class="rating-summary-block">
				 	by <a title="{{$product->brand}}" href="{{route("brand.view",[rawurlencode(strtolower($product->brand))])}}">{{$product->brand}}</a>
				 	@if(isset($review) && $review->aScore()>0)
				 	<span class="rating">
				 	    <strong class="hidden">Editor Rating: </strong> @include("product.parts.rating",['rate'=>round($review->aScore() * 20,0)]) <small>({{number_format($review->aScore(),2)}} out of 5)</small>
				 	</span>
				 	@endif
                </div>
				<div class=" main-action-btn">@include('product.parts.amzlink',['asin'=>$product->asin])</div>
				@if($features = $product->attribute("Feature"))
				{!! $features !!}
				@endif
		    </div>
		    <div class="col-md-3 col-sm-12">
		    @include("product.parts.summary")
		    </div>
		</div>
		<div class="row">
		<div class="col-md-9">
				@if( $review)
  				<div id="analysis" class="nav-tab-content">
  					@include("product.parts.analysis")
  				</div>
  				@endif

  				<div id="info" class="nav-tab-content">
  					@include("product.parts.moreinfo")
  				</div>
		</div>

		<div class="col-md-3 sidebar hidden-md hidden-sm hidden-xs">
		    @include("product.parts.related",['items'=>$product->relatedProducts()])
		</div>

	</div>
	<div class="related p-related" ></div>
  	<script>
	$(document).ready(function(){
		$(".p-related").load("{{route('product.related',[$product->asin])}}")
	})
	</script>
</div>

<script type="text/javascript">
@if(!empty($review) && !empty($review->most_used_words) && !empty(json_decode($review->most_used_words,true)))
jQl.loadjQdep('{{url('js/jqcloud.min.js')}}')
jQuery(document).ready(function(){
	<?php
		$words = [];
		foreach (json_decode($review->most_used_words,true) as $w) {
			$words[] = ["text"=>$w[0], "weight"=>$w[1]];
		}
	?>

	var words =<?php echo json_encode($words)?>;
	function initjQcloud(){
		if($('#most_words').jQCloud){

			$('#most_words').jQCloud(words,{

				 autoResize: true
				});
		} else {
			setTimeout( initjQcloud, 20 );
		}
	}
	initjQcloud()
})
@endif
</script>
@endsection
