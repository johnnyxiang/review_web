<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	@foreach($products as $product)
	<url>
		<loc>
			{{route('product.view',[ $product->product->slug])}}
		</loc>
		<changefreq>weekly</changefreq>
   		<priority>0.8</priority>
		
	</url>
	@endforeach
</urlset>