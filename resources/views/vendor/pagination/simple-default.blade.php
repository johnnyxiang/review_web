@if ($paginator->hasPages())
    <ul class="list-inline pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
<!--             <li class="disabled"><span>@lang('pagination.previous')</span></li> -->
        @else
            <li><a class="btn btn-default btn-lg" href="{{ $paginator->previousPageUrl() }}" rel="prev">« Prev Page</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a class="btn btn-default btn-lg next"  href="{{ $paginator->nextPageUrl() }}" rel="next">Next Page »</a></li>
        @else
<!--             <li class="disabled"><span>@lang('pagination.next')</span></li> -->
        @endif
    </ul>
@endif
