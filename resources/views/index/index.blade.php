@if(!$ajax)
@extends('layouts.home')
@section('title','Welcome to ReviewShoppingGuide.com')
@section('meta_description','ReviewShoppingGuide.com delivers you best and top products you are looking for, with merging data analysis technologies and experienced editors.')

@section('content')
@endif

<div class="jumbotron">
      <div class="container">
        <h1>View Our Latest Picks</h1>
        <p>ReviewShoppingGuide.com delivers you best and top products you are looking for, with merging data analysis technologies and experienced editors.</p>
       
      </div>
</div>

<div class="container jscroll">
<div class="row jscroll-content">
	@foreach($posts as $post)
	<div class="col-sm-3 home-post-block">
		<div class="img-container" style="position:relative;display:block;padding-bottom:100%">
			<a title="{{$post->title()}}" class="img-link" href="{{$post->link()}}">
				<img class="img-responsive lazy" alt="{{$post->title()}}" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-original="{{$post->image()}}">
			</a>
		</div>
		<div class="title-container">
		<div class="title-container-bg"></div>
		<h2><a href="{{$post->link()}}" title="{{$post->title()}}">{{$post->title()}}</a>
		@if (Auth::check() && Auth::user()->role == 'admin')
		<a  onclick="return confirm('Please confirm!')" href="{{ route('category.remove_feature',[$post->id])}}">Remove </a>
		@endif
		</h2>
		
		</div>
	</div>	
	@endforeach
	
	<div class="hidden">
	{{ $posts->links() }}
	</div>
</div>

</div>
@if(!$ajax)
@endsection
@endif