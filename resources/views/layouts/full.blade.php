<!DOCTYPE html>
<html lang="en">
  <head>
  
   	 @include('layouts.parts.head')
   


    </head>
    <body class="column-1">
    
    @include('layouts.parts.nav')
    <div class="container-fluid main" >
      <div class="">
    	<div class="main-content" >
      
       		
            @yield('content')
        
        </div>
        
        
     </div></div>
     
     
     
   @include('layouts.parts.footer')
        
    </body>
</html>