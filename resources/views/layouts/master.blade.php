<!DOCTYPE html>
<html lang="en">
  <head>
  
   	 @include('layouts.parts.head')
   

    </head>
    <body>
    
    @include('layouts.parts.nav')
    <div class="container main" >
      <div class="row ">
    	<div class="col-md-9 main-content" >
      
       		
            @yield('content')
        
        </div>
        
        <div class="col-md-3 sidebar">
        	
			
			@yield('sidebar')

        </div>
     </div></div>
     
     
     
   @include('layouts.parts.footer')
        
    </body>
</html>