<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layouts.parts.head')

</head>
    <body class="homepage">
    
    @include('layouts.parts.nav')
    <div class="container-fluid main" >
      
      
       		
            @yield('content')
        
      </div>
     
     
     
    @include('layouts.parts.footer')
        
    </body>
</html>