<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{Request::root()}}" title="Review & Shopping Guide" style="color: black;font-weight: 700;font-size: 30px;letter-spacing: -2px">
              Review & Shopping Guide
          </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
        
        <form class="navbar-form navbar-right" action="{{route("search.result")}}" style="width:400px">
           
             <div class="input-group" style="width: 100%">
               <input type="text" name="q" id="searchField" class="form-control" placeholder="Search ..."/>
            
           		<div class="input-group-btn">
            		<button type="submit" class="btn btn-default">Search</button>
            	</div>
            </div>
        </form>
        </div>
      </div>
</nav>