 <div class="footer ">
  <div class="container">
  <div class="row">
 	  <div class="col-sm-8 col-md-9">
	      <ul class='nav navbar-nav'>
	        
	        <li><a title="Privacy Policy" href="{{route('cms.page',['privacy-policy'])}}">Privacy Policy</a></li>
	        <li><a title="Terms and Conditions"  href="{{route('cms.page',['terms'])}}">Terms and Conditions</a></li>
	        <li><a title="Contact Us"  href="{{route('cms.page',['contacts'])}}">Contact Us</a></li>
	        <li><a title="Disclaimers"  href="{{route('cms.page',['disclaimers'])}}">Disclaimers</a></li>
            <li> </li>               
        </ul>
        
        <div class="pull-right" style="padding: 15px 15px;">Copyright &copy; {{date('Y')}} ReviewShoppingGuide.com </div>
	    </div>
	  
	    
</div>

@if (Auth::check())
<a href="{{ route('logout')}}">Log out</a>  
@endif 
  </div>
  
</div>
<script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=dc466044-1ba3-42f0-a90b-845e1d470f3c"></script>
@include('layouts.parts.script')






