<script type="text/javascript">
jQuery(document).ready(function(){

	function initLazyload(){
		if($("img.lazy").lazyload){
		 	$("img.lazy").lazyload()
		} else {
			setTimeout( initLazyload, 20 );
		}
	}

	initLazyload()
	
	function initInfiniteScroll(){
		
		if($.jscroll) {
			$('.jscroll').each(function(){
				$(this).jscroll({
					padding: 20,
				    nextSelector: '.pagination a.next',
				    padding:100,
				    contentSelector:'.jscroll-content',
				    callback:function(){
				    	initLazyload()
					  }
				});

				
			})
		} else {
			setTimeout( initInfiniteScroll, 20 );
		}
	
	}

	initInfiniteScroll()
})
</script>

 
<noscript id="deferred-styles">
     
      {{ Html::style('js/fotorama/fotorama.css') }}
      <link href="//cdn.rawgit.com/noelboss/featherlight/1.7.6/release/featherlight.min.css" type="text/css" rel="stylesheet" />
</noscript>
<script>
      var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);
</script>