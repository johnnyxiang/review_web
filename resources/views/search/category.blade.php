@extends('layouts.master')


@section('title',"Top Rated $keyword Products We Recommend" )
@section('meta_description',"Check out top rated $keyword products ReviewShoppingGuide.com recommends for you.")




@section('content')
<?php 
if($limit > count($categories)) $limit = count($categories);
?>

@if ($limit == 0)
<h1>Sorry, we didn't find any products for "{{trim($keyword,'"')}}".</h1>
<br/><br/><br/><br/>
@else
<h1 class="title">
	
	Top Rated <span>{{trim($keyword,'"')}}</span> Categories We Recommend
	

</h1>
        
  
<div class="row">
	@foreach($categories as $category)
	@if(!empty($category->image()))
	<div class="col-sm-4 home-post-block">
		<div class="img-container" style="position:relative;display:block;padding-bottom:100%">
			<a title="{{$category->name()}}" class="img-link" href="{{$category->link()}}" style="background-image: url({{$category->image()}})">
				&nbsp;
			</a>
		</div>
		<div class="title-container">
		<div class="title-container-bg"></div>
		<h2><a href="{{$category->link()}}" title="Top 5 Best  {{$category->name()}}">Top 5 Best {{$category->name()}}</a>
		
		</h2>
		
		</div>
	</div>
	@endif	
	@endforeach

</div>
   
		
@endif		
	


@endsection

@section('sidebar')
@include('product.parts.ad')
@endsection

