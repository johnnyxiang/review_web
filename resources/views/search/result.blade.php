@extends('layouts.full')

@if(isset($type) && $type == "brand")
@section('title',"Top Rated $keyword Products We Recommend" )
@section('meta_description',"Check out top rated $keyword products ReviewShoppingGuide.com recommends for you.")
@else
@section('title',"Top Rated Products for $keyword We Recommend" )
@section('meta_description',"Looking for ".$keyword."? Check out top rated products for $keyword ReviewShoppingGuide.com recommends for you.")

@endif



@section('content')
<?php 
if($limit > count($reviews)) $limit = count($reviews);
?>

@if ($limit == 0)
<h1>Sorry, we didn't find any products for "{{trim($keyword,'"')}}".</h1>
<br/><br/><br/><br/>
@else
<h1 class="title">
	@if(isset($type) && $type == "brand")
	Top Rated <span>{{trim($keyword,'"')}}</span> Products We Recommend
	@else
	Top Rated Products for "{{trim($keyword,'"')}}" We Recommend
	@endif
	
	@if (Auth::check() && Auth::user()->role == 'admin')
	<a href="{{route('search.feature',[$keyword,$reviews[0]->asin])}}" click="return confirm('Please confirm!')" class="btn btn-primary pull-right">Feature on Homepage</a>
	@endif
</h1>
        
    @include ('product.parts.table')
<div class="container">
    @foreach($reviews as $i=> $review)
		@include ('product.parts.card')
	@endforeach  
</div>
@endif		
@include('product.parts.amzads')	
@endsection


