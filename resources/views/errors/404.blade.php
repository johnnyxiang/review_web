@extends('layouts.home')
@section('title','404 not found')
@section('meta_description','404 not found')
@section('content')

<div class="container">

<h1 style="margin-bottom: 50px">Page not found.</h1>


<p>Oops! Something went wrong and we couldn't find the page you were looking for.</p>
<p><a href="{{url("/")}}">Click here to return to home page.</a></p>        
</div>
@endsection
