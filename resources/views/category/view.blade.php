@extends('layouts.full')
@section('meta_description',$category->desc?$category->desc:"Looking for ".$category->name()."? Check out the top $limit best ".$category->name()." products of ".date("M Y")." in ".implode(", ",$ancestors)." ReviewShoppingGuide.com recommends for you.")
@section('title',"Top $limit Best ".$category->name()." Products " )

@section('content')
		
@if ($limit == 0)
<h1>Sorry, we didn't find any products in <span>{{$category->name()}}</span> category.</h1>
<br/><br/><br/><br/>	
@else
<h1 class="title">Top {{$limit}} Best <span>{{$category->name()}}</span> of {{date("M Y")}}
@if (Auth::check() && Auth::user()->role == 'admin')
<a href="{{route('category.feature',[$category->id])}}" click="return confirm('Please confirm!')" class="btn btn-primary pull-right">Feature on Homepage</a>
@endif

</h1>
@endif 

@if($desc = $category->desc())
   <div class="desc">{!! $category->desc() !!}</div>
@endif       
       
@include ('product.parts.table')

<div class="container">
@foreach($reviews as $i=>$review)
	@include ('product.parts.card')
@endforeach

@include('product.parts.amzads')
</div>
@endsection
