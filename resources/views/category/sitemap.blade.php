<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	@foreach($categories as $cat)
	<url>
		<loc>
			{{route('category.view',[ $cat->slug])}}
		</loc>
		<changefreq>weekly</changefreq>
   		<priority>0.9</priority>
		
	</url>
	@endforeach
</urlset>