@extends('layouts.home')
@section('title',$page->title)
@section('content')
<div class="container">
<h1>{{$page->title}}</h1>
<br/>
{!! $page->content !!}
</div>
@endsection